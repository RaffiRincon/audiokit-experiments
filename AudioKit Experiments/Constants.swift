//
//  Constants.swift
//  AudioKit Experiments
//
//  Created by Rafael Rincon on 12/7/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import Foundation

struct K {
    
    struct Note {
        static let defaultFrequencyA4 = Double(440)
        static let twelvthRootOf2 = Double(1.059463)
    }
    
}

func log(base: Float, of x: Float) -> Float {
    return log(x)/log(base)
}
