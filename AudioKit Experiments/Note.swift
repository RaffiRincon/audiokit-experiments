//
//  Note.swift
//  AudioKit Experiments
//
//  Created by Rafael Rincon on 12/7/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import UIKit

struct Note: Hashable {
    
    static func == (lhs: Note, rhs: Note) -> Bool {
        return lhs.frequency == rhs.frequency
    }
    
    
    enum NoteLetter: Int {
        case c = 0, d = 1, e = 2, f = 3, g = 4, a = 5, b = 6
        
        mutating func move(by d: Int) {
            self = NoteLetter(rawValue: (self.rawValue + d) % 7)!
        }
        
        static func -(lhs: NoteLetter, rhs: Int) -> NoteLetter {
            return NoteLetter(rawValue: lhs.rawValue + rhs)!
        }
        
        static func +(lhs: NoteLetter, rhs: Int) -> NoteLetter {
            return NoteLetter(rawValue: lhs.rawValue + rhs)!
        }
        
        static func <(lhs: NoteLetter, rhs: NoteLetter) -> Bool {
            return lhs.rawValue < rhs.rawValue
        }
        
        static func >(lhs: NoteLetter, rhs: NoteLetter) -> Bool {
            return lhs.rawValue > rhs.rawValue
        }
        
        static func ==(lhs: NoteLetter, rhs: NoteLetter) -> Bool {
            return lhs.rawValue == rhs.rawValue
        }
        
        static func !=(lhs: NoteLetter, rhs: NoteLetter) -> Bool {
            return lhs.rawValue != rhs.rawValue
        }
    }
    
    enum NotePitchModifier: Int {
        case sharp = 1, flat = -1, natural = 0
    }
    
    var semitonesAboveC: Int {
        var semitones = letter.rawValue * 2
        if semitones == 12 {
            semitones -= 2
        } else if semitones >= 3{
            semitones -= 1
        }
        
        semitones += pitchModifier.rawValue
        return semitones
    }
    
    var pitchModifier: NotePitchModifier!
    var letter: NoteLetter!
    var frequencyOfA4: Double!

    var octave: Int!
    var frequency: Double {
        var currentOctave = 4
        var currentAFrequency = frequencyOfA4!
        
        if currentOctave > octave {
            while currentOctave > octave {
                currentAFrequency /= 2
                currentOctave -= 1
            }
        } else if currentOctave < octave {
            while currentOctave < octave {
                currentAFrequency *= 2
                currentOctave += 1
            }
        }
        
        var currentFrequency = currentAFrequency
        
        let semitonesAboveA = semitonesAboveC - NoteLetter.a.rawValue
        let multiplier = pow(K.Note.twelvthRootOf2, Double(semitonesAboveA))
        currentFrequency *= multiplier
        
        return currentFrequency
    }
    
    init(letter: NoteLetter, pitchModifier: NotePitchModifier, octave: Int, frequencyOfA4: Double) {
        self.pitchModifier = pitchModifier
        self.letter = letter
        self.frequencyOfA4 = frequencyOfA4
        self.octave = octave
    }
    
}
