//
//  NotePlayer.swift
//  AudioKit Experiments
//
//  Created by Rafael Rincon on 12/7/18.
//  Copyright © 2018 Rafael Rincon. All rights reserved.
//

import Foundation
import AudioKit

class NotePlayer {
    private init() {
        try! AudioKit.start()
    }
    
    static let shared = NotePlayer()
    
    private var noteToOscillator = [Note: AKOscillator]()
    
    func addNoteOscillater(_ note: Note) {
        let table = AKTable(.sine)
        let oscillator = AKOscillator(
            waveform: table,
            frequency: note.frequency,
            amplitude: 1,
            detuningOffset: 0,
            detuningMultiplier: 1
        )
        
        noteToOscillator[note] = oscillator
    }
    
    func playNote(_ note: Note) {
        var oscillator = noteToOscillator[note]
        if oscillator == nil {
            addNoteOscillater(note)
            oscillator = noteToOscillator[note]
        }
        
        AudioKit.output = oscillator
        
        oscillator?.start()
    }
    
    deinit {
        try! AudioKit.stop()
    }
}
